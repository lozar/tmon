# TMon
## Overview
TMon (Tiny Monad) is some monad helper functions for F# implementing `map (<!>)`, `apply (<*>)` and `bind (>>=)` operator for `Maybe<'a>` (like Option), `Result<'a,'b>` and `AR<'a,'b>`.

This is a combination of material I found on using static type constraints for the operators, supplemented with `traverse` and `sequence` from [Scott Wlaschin's blog](https://fsharpforfunandprofit.com/) that is extended to the new types found here.


## Building:
```bash
rm -r  */bin */obj */publish
dotnet restore
dotnet build
dotnet test Tests
dotnet pack -o publish TMon/TMon.fsproj
```

Need to add automated build/nuget push and details here on how that happens. I don't expect this to change much. It accomodates the limited needs I have for it in a couple of projects.
