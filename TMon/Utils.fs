namespace TMon

module Utils =

    open System
    open System.Dynamic
    open System.Collections.Generic
    open Microsoft.FSharp.Reflection

    type LogLevel = Severe | Error | Info

    let private log level msg =
        let fmt = "MM/dd hh:mm:ss:FFF";
        printfn "%s: %A: %A" (DateTime.Now.ToString(fmt)) level msg

    let severe obj = log Severe obj

    let error obj = log Error obj

    let info obj = log Info obj

    let logResult = function
        | Success s -> sprintf "Success (%A)" s |> info
        | Fail f    -> sprintf "Fail (%A)" f |> error

    let toGenericList lst = List<_>(List.toSeq lst)

    let mapToDictionary param =
        let dict = ExpandoObject() :> IDictionary<string,obj>
        (dict, param)
        ||> Map.fold (fun acc k v -> acc.Add(k, v); acc)

    let cfg = 
        let envVars = 
            Environment.GetEnvironmentVariables()
            |> Seq.cast<Collections.DictionaryEntry>
            |> Seq.map (fun d -> string d.Key, string d.Value)

        fun key -> Map.tryFind key (envVars |> Map.ofSeq)

    let cfgDef defVal key = Option.defaultValue defVal (cfg key)

    let flip f x y = f y x

    let inline runtimeMillis (test: unit -> ^a): ^a * int64 =
        let watch = System.Diagnostics.Stopwatch.StartNew()
        let result = test()
        watch.Stop()
        result, watch.ElapsedMilliseconds

    let extract<'T> (o: obj) = 
        let dict = o :?> IReadOnlyDictionary<string,obj>
        let flds = FSharpType.GetRecordFields(typeof<'T>)
        let vals = [| for f in flds -> dict.[f.Name] |]
        FSharpValue.MakeRecord(typeof<'T>, vals) :?> 'T
